export default {
  // byIP(ip) {
  //   const ip_address = ip || "194.186.207.248";
  //   const auth = "2e8de3b5-4635-40e9-8b27-2a3bb30cbb1d";
  //   const url = "https://ipfind.co/?auth=" + auth + "&ip=" + ip_address;

  //   return this.httpclient.get(url);
  // },
  byGPS() {
    return new Promise((resolve, reject) => {
      if (navigator.geolocation) {
        return navigator.geolocation.getCurrentPosition(
          (position) => {
            const long = position.coords.longitude;
            const lat = position.coords.latitude;
            console.log(long, lat);
            resolve({ y: long, x: lat });
          },
          (err) => reject(err.message),
        );
      }
      return reject(new Error('No support for geolocation'));
    });
  },
};
