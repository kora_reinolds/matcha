export default {
  data: () => ({
    startLen: 0,
  }),
  methods: {
    curLen(list, path) {
      const len = list.length || list.keys().length;
      if (this.$route.path === `/${path}`) {
        this.startLen = len;
      }
      return len - this.startLen;
    },
  },
};
