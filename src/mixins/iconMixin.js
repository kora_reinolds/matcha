export default {
  data: () => ({
    icons: {
      like: 'heart',
      dislike: 'heart-broken',
      visit: 'eye',
      message: 'comment-alt',
      male: 'mars',
      female: 'venus',
      bisexual: 'transgender',
    },
  }),
};
