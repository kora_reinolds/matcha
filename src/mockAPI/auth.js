export default {
  saveChanges() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 1000);
    });
  },
  login() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          id: 10,
          dist: null,
          time: Date.now(),
          token: 'token',
          firstName: '',
          lastName: 'petrov',
          mail: 'vasya@mail.ru',
          age: 44,
          gender: 'male',
          preferences: ['male'],
          biography: 'Too much text about vasya life,Too much text about vasya life Too much text about vasya life Too much text about vasya life Too much text about vasya life Too much text about vasya life Too much text about vasya life Too much text about vasya life v v Too much text about vasya life Too much text about vasya life',
          tags: ['football', 'TV', 'poker', 'fishing'],
          pictures: [213], // list urls
          fameRating: 500,
          location: [55.7964456, 37.5746487],
          visitorList: [],
          likeList: [],
          mailConfirmed: true,
          informationFilled: true,
        });
      }, 1000);
    });
  },
};
