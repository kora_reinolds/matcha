import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import UserList from '../views/UserList.vue';
import Settings from '../views/Settings.vue';
import History from '../views/History.vue';
import Chat from '../views/Chat.vue';
import NotFound from '../components/NotFound.vue';
import UserPage from '../views/UserPage.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    // meta: { transitionName: 'login' },
  },
  {
    path: '/user/:id',
    name: 'user',
    component: UserPage,
    // meta: { transitionName: 'user_page' },
  },
  {
    path: '/search',
    name: 'main',
    // component: () => import(/* webpackChunkName: "main" */ ),
    component: UserList,
  },
  {
    path: '/settings',
    name: 'settings',
    // component: () => import(/* webpackChunkName: "settings" */ ),
    component: Settings,
  },
  {
    path: '/visitors',
    name: 'visitors',
    // component: () => import(/* webpackChunkName: "history" */ ),
    component: History,
  },
  {
    path: '/history',
    name: 'history',
    // component: () => import(/* webpackChunkName: "history" */ ),
    component: History,
  },
  {
    path: '/notifications',
    name: 'notifications',
    // component: () => import(/* webpackChunkName: "notifications" */ ),
    component: History,
  },
  {
    path: '/chat',
    name: 'chat',
    // component: () => import(/* webpackChunkName: "chat" */ ),
    component: Chat,
  },
  { path: '*', component: NotFound },
];

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => {
  if (localStorage.getItem('user')) {
    if (to.path === '/' && from.path !== '/') next(from.path);
    else if (to.path === '/' && from.path === '/') next('/search');
    else next();
  } else if (to.path !== '/') next('/');
  else next();
});


export default router;
