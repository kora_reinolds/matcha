import Vue from 'vue';
import Vuex from 'vuex';
import API from '../mockAPI/userGenerator';
import usersModule from './modules/users';
import messagesModule from './modules/messages';
import toolsModule from './modules/tools';
import router from '../router';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    mobile: window.innerWidth <= 480,
    user: null,
    token: localStorage.getItem('user'),
    changes: {},
  },
  getters: {
    IS_MOBILE: (state) => state.mobile,
    IMAGES: (state) => (state.user ? state.user.pictures : ''),
    TAGS: (state) => (state.user ? state.user.tags : ''),
    BIOGRAPHY: (state) => (state.user ? state.user.biography : ''),
    PREFERENCES: (state) => (state.user ? state.user.preferences : ''),
    GENDER: (state) => (state.user ? state.user.gender : ''),
    MAIL: (state) => (state.user ? state.user.mail : ''),
    AGE: (state) => (state.user ? state.user.age : ''),
    LAST_NAME: (state) => (state.user ? state.user.lastName : ''),
    FIRST_NAME: (state) => (state.user ? state.user.firstName : ''),

    USER_ID: (state) => state.user.id,
    USER: (state) => state.user,
    LOCATION: (state) => (state.user ? state.user.location : ''),
    USER_AUTH: (state) => state.token,
    CHANGES: (state) => state.changes,
    CHANGES_EXIST: (state) => Object.keys(state.changes).length,
    EMPTY_FIELD: (state) => Object.values(state.user).includes(null),
  },
  mutations: {
    RESIZE: (state) => {
      console.log('resize');
      state.mobile = window.innerWidth <= 480;
    },
    INFORMATION_FILL: (state) => { state.user.informationFilled = true; },
    SET_USER: (state, user) => {
      state.user = user;
      state.user = { ...state.user };
    },
    REMOVE_USER: (state) => { state.user = null; },
    SET_VALUE: (state, payload) => {
      state.user[payload.key] = payload.val;
    },
    DELETE_IMAGE_CHANGE: (state, id) => {
      if (id < 0) {
        const index = state.changes.newImages.findIndex((img) => img.id === id);
        state.changes.newImages.splice(index, 1);
      } else if (state.changes.deleteImages) {
        state.changes.deleteImages.push(id);
      } else {
        state.changes.deleteImages = [id];
      }
    },
    SET_IMAGE_CHANGE: (state, dataImg) => {
      if (state.changes.newImages) {
        state.changes.newImages.push(dataImg);
      } else {
        state.changes.newImages = [dataImg];
      }
      state.changes = { ...state.changes };
    },
    SET_CHANGE: (state, payload) => {
      state.changes[payload.key] = payload.val;
      state.changes = { ...state.changes };
    },
    CLEAR_CHANGE: (state, payload) => {
      delete state.changes[payload];
    },
  },
  actions: {
    SAVE_CHANGES({ commit, getters }) {
      API.saveChanges(getters.CHANGES).then(() => {
        commit('INFORMATION_FILL');
        router.push({ path: `/user/${getters.USER_ID}` });
      });
    },
    LOGOUT({ commit }) {
      commit('REMOVE_USER');
      localStorage.removeItem('user');
      router.push({ name: 'login' });
    },
    LOGIN({ commit }) {
      API.getUsers(1000).then((data) => {
        localStorage.setItem('user', data.token);
        commit('SET_USER', data.user);
        commit('tools/SET_SEARCH_PARAMS', data.user, { root: true });
        commit('users/SET_USERS', data);
        commit('tools/SET_USERS', data, { root: true });
        commit('tools/FILTER_USERS', null, { root: true });

        if (data.user.informationFilled) router.push({ path: `/user/${data.user.id}` });
        else router.push({ path: '/settings' });
      });
    },
  },
  modules: {
    messages: messagesModule,
    users: usersModule,
    tools: toolsModule,
  },
});
